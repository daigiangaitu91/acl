<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 05/12/2015
 * Time: 3:57 CH
 */
return array(
    'Facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('CALLBACK_URL'),
    ],
);