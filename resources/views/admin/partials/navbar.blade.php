<ul class="nav navbar-nav">

    @if (Auth::check())
        {{--*/ $roles = Auth::user()->roles()->lists('role_name')->all() /*--}}
        @if (Auth::user()->isAdmin())
            <li @if (Request::is('admin/user')) class="active" @endif>
                <a href="/admin/user">Manage User</a>
            </li>
            <li @if (Request::is('admin/role')) class="active" @endif>
                <a href="/admin/role">Manage Role</a>
            </li>
            <li @if (Request::is('admin/permission')) class="active" @endif>
                <a href="/admin/permission">Manage Permission</a>
            </li>
        @endif
    @endif
</ul>
<ul class="nav navbar-nav navbar-right">
    @if (Auth::guest())
        <li><a href="/auth/login">Login</a></li>
    @else
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
               aria-expanded="false">{{ Auth::user()->name }}
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="/auth/logout">Logout</a></li>
            </ul>
        </li>
    @endif
</ul>