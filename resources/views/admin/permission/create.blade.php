<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 27/11/2015
 * Time: 7:52 CH
 */
?>
@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create role</div>
                    <div class="panel-body">

                        @include('admin.partials.errors')

                        <form class="form-horizontal" role="form" method="POST"
                              action="{{ url('/admin/permission') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            @include('admin.permission._partials._form')
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Create permission</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
