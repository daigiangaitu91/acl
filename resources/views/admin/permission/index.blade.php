<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 30/11/2015
 * Time: 9:11 CH
 */
        ?>
@extends('admin.layout')
@section('content')
    <div class="container-fluid">
        <div class="row page-title-row">
            <div class="col-md-6">
                <h3>Permissions</small></h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="/admin/permission/create" class="btn btn-success btn-md">
                    <i class="fa fa-plus-circle"></i> New create
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <!--
        @include('admin.partials.errors')
                @include('admin.partials.success') -->

                <table id="tags-table" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Permission name</th>
                        <th>Permission slug</th>

                        <th>Permission description</th>
                        <th>Permission role</th>

                        <th data-sortable="false">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($permissions as $permission)
                        <tr>
                            <td>{{ $permission['permission_title'] }}</td>
                            <td>{{ $permission['permission_slug'] }}</td>
                            <td>{{ $permission['permission_description'] }}</td>

                            <td>
                                @foreach($permission['roles'] as $role)
                                    {{ $role['role_name'] }} <br/>
                                    @endforeach
                            </td>
                            <td>
                                <a href="/admin/permission/{{ $permission['id'] }}/edit"
                                   class="btn btn-xs btn-info">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function() {
            $("#tags-table").DataTable({
            });
        });
    </script>
@stop


