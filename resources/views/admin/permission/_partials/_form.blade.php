
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label for="title" class="col-md-2 control-label">
                Title
            </label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="permission_title" autofocus
                       id="title" value="{{ $permission_title }}">
            </div>
        </div>

        <div class="form-group">
            <label for="subtitle" class="col-md-2 control-label">
                Slug
            </label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="permission_slug"
                       id="subtitle" value="{{ $permission_slug }}">
            </div>
        </div>

        <div class="form-group">
            <label for="content" class="col-md-2 control-label">
                Description
            </label>
            <div class="col-md-10">
        <textarea class="form-control" name="permission_description" rows="14"
                  id="content">{{ $permission_description }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="tags" class="col-md-3 control-label">
                Tags
            </label>
            <div class="col-md-8">
                <select name="roles[]" id="roles" class="form-control" multiple>

                    @foreach ($allRoles as $key=>$role)

                        <option @if (in_array($role, $roles)) selected @endif
                        value="{{ $key }}">
                            {{ $role }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

</div>
