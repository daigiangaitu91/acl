
@extends('admin.layout')

@section('content')

    <div class="container-fluid">
        <div class="row page-title-row">
            <div class="col-md-6">
                <h3>Users <small>» Listing</small></h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="/admin/user/create" class="btn btn-success btn-md">
                    <i class="fa fa-plus-circle"></i> New user
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
        @include('admin.partials.errors')
                @include('admin.partials.success')
                <table id="tags-table" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>User name</th>
                        <th>Email</th>
                        <th class="hidden-sm">First name</th>
                        <th class="hidden-md">Last name</th>
                        <th class="hidden-md">Phone number</th>
                        <th class="hidden-md">Linked</th>
                        <th class="hidden-sm">Address</th>
                        <th data-sortable="false">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)

                        <tr>
                            <td>{{ $user['name'] }}</td>
                            <td>{{ $user['email'] }}</td>
                            <td class="hidden-sm">{{ $user['profile']['firstname'] }}</td>
                            <td class="hidden-md">{{ $user['profile']['lastname'] }}</td>
                            <td class="hidden-md">{{ $user['profile']['phone_number'] }}</td>
                            <td class="hidden-md">{{ $user['profile']['linked'] }}</td>
                            <td class="hidden-sm">{{ $user['profile']['address']  }}
                            </td>
                            <td>
                                <a href="/admin/user/{{ $user['id'] }}/edit"
                                   class="btn btn-xs btn-info">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop


