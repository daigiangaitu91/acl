<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 16/11/2015
 * Time: 9:35 CH
 */
        ?>
        <!-- /resources/views/users/edit.blade.php -->

@extends('admin.layout')
@section('content')

    <div class="portlet box green">
        <div class="portlet-title">

            <div class="tools">
                <a class="collapse" href="javascript:;" data-original-title="" title="">
                </a>
                <a class="config" data-toggle="modal" href="#portlet-config" data-original-title="" title="">
                </a>
                <a class="reload" href="javascript:;" data-original-title="" title="">
                </a>
                <a class="remove" href="javascript:;" data-original-title="" title="">
                </a>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            {{ddd($user)}}
            {!! Form::model($user, ['method' => 'PATCH', 'route' => ['users.update', $user->id]]) !!}
            @include('users/partials/_form', ['submit_text' => 'Edit user'])
            {!! Form::close() !!}
                    <!-- END FORM-->
        </div>
    </div>
@endsection
