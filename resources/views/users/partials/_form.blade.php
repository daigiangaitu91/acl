<!-- /resources/views/users/partials/_form.blade.php -->

<form class="form-horizontal" role="form" method="POST" action="/user/store">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-body">
        <div class="form-group">
            <label class="col-md-3 control-label">Text</label>
            <div class="col-md-4">
                {!! Form::text('name',$user->name,array('class' => 'form-control input-circle')) !!}
                <span class="help-block">
					A block of help text.
				</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Email Address</label>
            <div class="col-md-4">
                <div class="input-group">
					<span class="input-group-addon input-circle-left">
						<i class="fa fa-envelope"></i>
					</span>
                    {!! Form::email('email',$user->email,array('class' => 'form-control input-circle-right'))!!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Password</label>
            <div class="col-md-4">
                <div class="input-group">
                    {!! Form::password('password',array('class' => 'form-control input-circle-left')) !!}
                    <span class="input-group-addon input-circle-right">
						<i class="fa fa-user"></i>
					</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Confirm password</label>
            <div class="col-md-4">
                <div class="input-icon">
                    <i class="fa fa-bell-o"></i>
                    {!! Form::password('password_confirmation',array('class' => 'form-control input-circle')) !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">First name</label>
            <div class="col-md-4">
                <div class="input-icon right">
                    {!! Form::text('firstname',$user->firstname,array('class' => 'form-control input-circle')) !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Last name</label>
            <div class="col-md-4">
                {!! Form::text('lastname',$user->lastname,array('class' => 'form-control input-circle')) !!}
            </div>
        </div>
        <div class="form-group last">
            <label class="col-md-3 control-label">Role id</label>
            <div class="col-md-4">
                {!! Form::select('role_id', array('1' => 'superadmin', '2' => 'siteadmin','3' => 'normal'),array('class' =>'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-circle blue" type="submit">Submit</button>
                <button class="btn btn-circle default" type="button">Cancel</button>
            </div>
        </div>
    </div>
</form>
<!-- END FORM-->
