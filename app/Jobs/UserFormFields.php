<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Profile;
use App\Roles;
use App\User;
use Illuminate\Contracts\Bus\SelfHandling;

class UserFormFields extends Job implements SelfHandling
{
    /**
     * The id (if any) of the Post row
     *
     * @var integer
     */
    protected $id;

    /**
     * Create a new command instance.
     *
     * @param integer $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * Execute the command.
     *
     * @return array of fieldnames => values
     */
    public function handle()
    {
        $fields = [];
        if ($this->id) {
            $user = User::findOrFail($this->id);
            if($user->profile->get()){
                $user->profile->get();

                foreach (array_keys($user->fields_profile()) as $field) {
                    $fields[$field] = old($field, $user->profile->$field);
                }
            }
            $fields['id'] = $user->id;
        } else {
            $user = new User();
            $profile = new Profile();

            foreach (array_keys($user->fields_profile()) as $field) {
                $fields[$field] = old($field, $profile->$field);
            }
        }
            foreach (array_keys($user->fields_user()) as $field) {
                $fields[$field] = old($field, $user->$field);
            }

            $fields['roles'] = $user->roles()->lists('role_name')->all();
        return array_merge(
            $fields,
            ['allRoles' => Roles::lists('role_name','id')->all()]
        );
    }
}
