<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permission;
class Roles extends Model
{
    protected $table = 'roles';

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function permissions(){
        return $this->belongsToMany('App\Permission');
    }
    
    public static function addNeededRoles(array $roles)
    {
        if (count($roles) === 0 ){
            return;
        }
        $found = static::whereIn('id',$roles)->lists('id')->all();

        foreach (array_diff($roles, $found) as $role) {

            static::create([
                'role' => $role,


            ]);
        }
    }
}
