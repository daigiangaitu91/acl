<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'firstname','lastname','phonenumber','linked','address', 'active_code', 'is_active'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
